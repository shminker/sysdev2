from django.urls import path
from . import views

urlpatterns = [
    path('', views.index, name='task'),
    path('getTask', views.getTask, name='test'),
    path('addTask', views.addTask, name='test'),
    path('addActivity', views.addActivity, name='test'),
    path('edit/<taskID>', views.editTask, name='test'),
    path('delete/<taskID>', views.deleteTask, name='test'),
    path('open/<taskID>', views.openTask, name='test'),
    path('getDates', views.getDates, name='test'),



]
