from django.shortcuts import render
from django.http import HttpResponse
from django.http import JsonResponse
from django.core import serializers
from datetime import datetime, timezone

import json


from root.models import *

def index(request):
    return openTask(request, 0)


def getTask(request): 
    if request.method == 'GET':
        task = Task.objects.get(id=request.GET['id'])
        #modules = Module.objects.all()
        taskActivities = ''
        activities = Activity.objects.filter(task_id=task.id)
        if len(activities) > 0:
            for activity in activities:
                taskActivities += '<div class="studyItem" onclick="getActivity(' + str(activity.id) + ')">\n'
                taskActivities += '<h2 class="studyItemTitle">' + activity.name + '</h2>\n'
                taskActivities += '</div>\n'

        context = {
            'id': task.id,
            'name':task.name,
            'set':task.set.strftime('%Y-%m-%dT%H:%M'),
            'due':task.due.strftime('%Y-%m-%dT%H:%M'),
            'note' : task.note,
            'assignment' : task.assignment.name,
            'activities' : taskActivities
        }
        return HttpResponse(json.dumps(context), content_type="application/json")
    return index(request)    

def addTask(request):
    if request.method == 'GET':
        taskName = request.GET['name'] #1.get the value of input named 'name' from form.
        taskSet = request.GET['set']
        taskDue = request.GET['due']
        taskNotes = request.GET['note']
        taskDuration = 0
        taskAssignment = Assignment.objects.get(id=request.GET['assignment'])

        dtSet = datetime.strptime(taskSet, "%Y-%m-%dT%H:%M")
        dtDue = datetime.strptime(taskDue, "%Y-%m-%dT%H:%M")
        taskDuration = dtDue - dtSet 
        print(taskDuration)
        #duration = taskDue - taskSet
        newTask = Task(
            name=taskName,
            set=taskSet,
            due=taskDue,
            note=taskNotes,
            duration=taskDuration,
            complete=0,
            assignment=taskAssignment,
            user_id=request.user.id
        )
        newTask.save()
    return HttpResponse(json.dumps("gg"), content_type="application/json")

def editTask(request, taskID):
    if request.method == 'GET':
        task = Task.objects.get(id=request.GET['id'])
        task.name = request.GET['name']
        task.set = request.GET['set']
        task.due = request.GET['due']
        task.note = request.GET['note']
        task.save()
        print(task.name)
    return HttpResponse(json.dumps("gg"), content_type="application/json")

def deleteTask(request, taskID):
    if request.method == 'GET':
        task = Task.objects.get(id=request.GET['id'])    
        task.delete()
    return HttpResponse(json.dumps("gg"), content_type="application/json")

def addActivity(request):
    if request.method == 'GET':
        task = Task.objects.get(id=request.POST['id'])
        task.name = request.POST['name']
        task.set = request.POST['set']
        task.due = request.POST['due']
        task.note = request.POST['note']
    return HttpResponse("kl bro")

def openTask(request, taskID):
    current_user = request.user
    tasks = Task.objects.filter(user_id=current_user, complete=0)
    assignments = []
    modules = UserModules.objects.filter(user_id=current_user)
    for module in modules:
        assign = Assignment.objects.filter(module_id=module.id)
        for assignment in assign:
            if (assignment.due > datetime.now(timezone.utc)):
                assignments.append(assignment) 
    context= { # 2.we create a dictionary of items we want to send to the view.
        'taskCount' : len(tasks),
        'tasks' : tasks,
        'assignments' : assignments,
        'open' : taskID,
        'minDateTime' : datetime.now().strftime("%Y-%m-%dT%H:%M")
    }
    return render(request, 'task/task.html', context)

def getDates(request):
    if request.method == 'GET':
        assignment = Assignment.objects.get(id=request.GET['id'])
        minDateTime = datetime.now()
        if assignment.set > datetime.now(timezone.utc):
            minDateTime = assignment.set
        context = {
            'min' : minDateTime.strftime("%Y-%m-%dT%H:%M"),
            'max' : assignment.due.strftime("%Y-%m-%dT%H:%M")
        }
    return HttpResponse(json.dumps(context), content_type="application/json")