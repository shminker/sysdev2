from django.urls import path
from . import views

urlpatterns = [
    path('', views.index, name='activity'),
    path('<activityID>', views.openActivity, name='article-section'),
    path('add/<taskID>', views.addActivity, name='article-section'),
    path('new/<taskID>', views.newActivity, name='article-section'),
    path('active/<activityID>', views.doActivity, name='article-section'),

]
