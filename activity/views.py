from django.shortcuts import render
from root.models import *
from datetime import datetime, timedelta
from django.http import HttpResponse
import json



def index(request):
    return render(request, 'activity/activity.html')

def openActivity(request, activityID):
    activity = Activity.objects.get(id=activityID)
    print(activity.name)
    context = {
        'mode' : "open",
        'activity' : activity,
        'isActive' : 0
    }
    return render(request, 'activity/activity.html', context)


def newActivity(request, taskID):
    task = Task.objects.get(id=taskID)
    duration = []
    taskDuration = int((task.due - task.set).total_seconds() / 60.0)
    for x in range(0, taskDuration):
        if x % 15 == 0:
            duration.append(x)
    print(taskDuration)
    context = {
        'mode' : "add",
        'duration' : duration,
        'task' : task,
        'isActive' : 0
    }
    return render(request, 'activity/activity.html', context)

def addActivity(request,taskID) :
    if request.method == 'GET':
        newActivity = Activity( #2.create the new Profile.
            name=request.GET['name'],
            note=request.GET['note'],
            duration=timedelta(seconds = int(request.GET['duration'])*60),
            complete=0,
            task=Task.objects.get(id=taskID),
            user=request.user
        )
        newActivity.save()
        print("saved")
    return HttpResponse(json.dumps("gg"), content_type="application/json")

def doActivity(request, activityID):
    activity = Activity.objects.get(id=activityID) 
    activity.duration -= timedelta(seconds=1)  
    activity.save()
    return HttpResponse(json.dumps(str(activity.duration)), content_type="application/json")