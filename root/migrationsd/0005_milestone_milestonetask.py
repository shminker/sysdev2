# Generated by Django 2.1.7 on 2019-05-03 18:51

from django.db import migrations, models
import django.db.models.deletion


class Migration(migrations.Migration):

    dependencies = [
        ('root', '0004_auto_20190407_1710'),
    ]

    operations = [
        migrations.CreateModel(
            name='Milestone',
            fields=[
                ('id', models.AutoField(auto_created=True, primary_key=True, serialize=False, verbose_name='ID')),
                ('name', models.CharField(max_length=150)),
                ('set', models.DateTimeField()),
                ('due', models.DateTimeField()),
                ('complete', models.IntegerField()),
                ('assignment', models.ForeignKey(on_delete=django.db.models.deletion.CASCADE, to='root.Assignment')),
            ],
        ),
        migrations.CreateModel(
            name='MilestoneTask',
            fields=[
                ('id', models.AutoField(auto_created=True, primary_key=True, serialize=False, verbose_name='ID')),
                ('milestone', models.ForeignKey(on_delete=django.db.models.deletion.CASCADE, to='root.Milestone')),
                ('task', models.ForeignKey(on_delete=django.db.models.deletion.CASCADE, to='root.Task')),
            ],
        ),
    ]
