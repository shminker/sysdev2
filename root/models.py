from django.db import models
from datetime import timedelta
from django.contrib.auth.models import User

class Module(models.Model):
    name = models.CharField(max_length=150)
    set = models.DateTimeField()
    due = models.DateTimeField()

class Assignment(models.Model):
    module = models.ForeignKey(Module, on_delete=models.CASCADE)
    name = models.CharField(max_length=150)
    weight = models.IntegerField()
    set = models.DateTimeField()
    due = models.DateTimeField()
    complete = models.IntegerField()

class Milestone(models.Model):
    assignment = models.ForeignKey(Assignment, on_delete=models.CASCADE)
    name = models.CharField(max_length=150)
    set = models.DateTimeField()
    due = models.DateTimeField()
    complete = models.IntegerField()
    user = models.ForeignKey(User, on_delete=models.CASCADE)

class Task(models.Model):
    assignment = models.ForeignKey(Assignment, on_delete=models.CASCADE)
    name = models.CharField(max_length=150)
    set = models.DateTimeField()
    due = models.DateTimeField()
    duration = models.DurationField(null = True)
    note = models.CharField(max_length=500)
    complete = models.IntegerField()
    user = models.ForeignKey(User, on_delete=models.CASCADE)

class Activity(models.Model):
    task = models.ForeignKey(Task, on_delete=models.CASCADE)
    name = models.CharField(max_length=150)
    duration = models.DurationField(null = True)
    note = models.CharField(max_length=500)
    complete = models.IntegerField()
    user = models.ForeignKey(User, on_delete=models.CASCADE)

class UserModules(models.Model):
    user = models.ForeignKey(User, on_delete=models.CASCADE)
    module = models.ForeignKey(Module, on_delete=models.CASCADE)

class MilestoneTask(models.Model):
    milestone = models.ForeignKey(Milestone, on_delete=models.CASCADE)
    task = models.ForeignKey(Task, on_delete=models.CASCADE)


