from django.shortcuts import render
from root.models import *
from django.http import HttpResponse
from django.http.response import JsonResponse
from django.views.decorators.csrf import csrf_exempt
import json, csv
from datetime import datetime, timezone
from dashboard.views import index
@csrf_exempt
def getModules(request):
    if request.method == 'POST':
        allUserModules = UserModules.objects.filter(user_id=request.user.id)
        modules = []
        for um in allUserModules:
            module = Module.objects.get(id=um.module_id)
            modules.append([module.id, module.name])
        return HttpResponse(json.dumps(modules), content_type="application/json")
    return HttpResponse(json.dumps(""), content_type="application/json")

@csrf_exempt
def readProfile(request):
    data = {}
    csv_file = request.FILES["csv_file"]
    if not csv_file.name.endswith('.csv'):
        messages.error(request,'File is not CSV type')
        return HttpResponseRedirect(reverse("myapp:upload_csv"))
    #if file is too large, return
    if csv_file.multiple_chunks():
        messages.error(request,"Uploaded file is too big (%.2f MB)." % (csv_file.size/(1000*1000),))
        return HttpResponseRedirect(reverse("myapp:upload_csv"))

    file_data = csv_file.read().decode("utf-8")		

    lines = file_data.split("\n")
    #loop over the lines and save them in db. If error , store as string and then display
    for line in lines:						
        fields = line.split(",")
        if (fields[0] == "module"):
            newModule = Module(
                name=fields[1],
                set=datetime.strptime(fields[2], "%Y-%m-%d %H:%M"),
                due=datetime.strptime(fields[3], "%Y-%m-%d %H:%M"),
            )
            newModule.save()
            newUserModule = UserModules(
                user_id=request.user.id,
                module_id=newModule.id,
            )
            newUserModule.save()
        if (fields[0] == "assignment"):
            newAssgniment = Assignment(
                name=fields[1],
                module_id=Module.objects.filter(name=fields[2])[0].id,
                weight=fields[3],
                set=datetime.strptime(fields[4], "%Y-%m-%d %H:%M"),
                due=datetime.strptime(fields[5], "%Y-%m-%d %H:%M"),
                complete=0,
            )									
            newAssgniment.save()
    return index(request)


def uploadProfile(request):
    return render(request, 'dashboard/profile.html')

def demo(request):
    newUM = UserModules(
        user_id=request.user.id,
        module_id=1,
    )
    newUM.save()
    newUM = UserModules(
        user_id=request.user.id,
        module_id=2,
    )
    newUM.save()
    newUM = UserModules(
        user_id=request.user.id,
        module_id=3,
    )
    newUM.save()
    newUM = UserModules(
        user_id=request.user.id,
        module_id=4,
    )
    newUM.save()
    
    taskSet ='2019-05-01T09:00'
    taskDue ='2019-05-03T09:00'
    dtSet = datetime.strptime(taskSet, "%Y-%m-%dT%H:%M")
    dtDue = datetime.strptime(taskDue, "%Y-%m-%dT%H:%M")
    taskDuration = dtDue - dtSet

    newTask = Task(
        assignment_id = 1,
        name = 'Plan',
        set = '2019-05-01 09:00',
        due = '2019-05-03 09:00',
        duration= taskDuration,
        note='sample note',
        complete=1,
        user_id=request.user.id,
    )
    newTask.save()

    newTask = Task(
        assignment_id = 1,
        name = 'Develop',
        set = '2019-05-04 09:00',
        due = '2019-05-05 09:00',
        duration= taskDuration,
        note='sample note',
        complete=1,
        user_id=request.user.id,
    )
    newTask.save()

    newTask = Task(
        assignment_id = 2,
        name = 'Plan',
        set = '2019-05-01 09:00',
        due = '2019-05-03 09:00',
        duration= taskDuration,
        note='sample note',
        complete=1,
        user_id=request.user.id,
    )
    newTask.save()

    newTask = Task(
        assignment_id = 2,
        name = 'Develop',
        set = '2019-05-04 09:00',
        due = '2019-05-05 09:00',
        duration= taskDuration,
        note='sample note',
        complete=1,
        user_id=request.user.id,
    )
    newTask.save()

    newTask = Task(
        assignment_id = 4,
        name = 'Create World',
        set = '2019-05-01 09:00',
        due = '2019-05-03 09:00',
        duration= taskDuration,
        note='sample note',
        complete=1,
        user_id=request.user.id,
    )
    newTask.save()

    newTask = Task(
        assignment_id = 4,
        name = 'Make Sprites',
        set = '2019-05-04 09:00',
        due = '2019-05-05 09:00',
        duration= taskDuration,
        note='sample note',
        complete=1,
        user_id=request.user.id,
    )
    newTask.save()

    newTask = Task(
        assignment_id = 4,
        name = 'Make Sprites',
        set = '2019-05-04 09:00',
        due = '2019-05-05 09:00',
        duration= taskDuration,
        note='sample note',
        complete=1,
        user_id=request.user.id,
    )
    newTask.save()


    return index(request)
