from django.urls import path
from . import views

urlpatterns = [
    path('readProfile', views.readProfile, name='test'),
    path('getModules', views.getModules, name='get modules'),
    path('uploadProfile', views.uploadProfile, name='get modules'),
    path('demo', views.demo, name='get modules'),

]
