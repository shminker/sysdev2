from django.urls import path
from . import views

urlpatterns = [
    path('', views.index, name='milestone'),
    path('getMilestone', views.getMilestone, name='test'),
    path('getTasks', views.getTasks, name='test'),
    path('addTask', views.addTask, name='test'),
    path('saveMilestone', views.saveMilestone, name='test'),
    path('deleteMilestone', views.deleteMilestone, name='test'),


]
