from django.shortcuts import render
from root.models import *
from root.models import Milestone
from django.http import HttpResponse
from django.http.response import JsonResponse
from datetime import datetime, timezone

import json


def index(request):
    current_user = request.user
    milestones = Milestone.objects.filter(user=request.user)

    assignments = []
    modules = UserModules.objects.filter(user_id=current_user)
    for module in modules:
        assign = Assignment.objects.filter(module_id=module.id, complete=0)
        for assignment in assign:
            if (assignment.due > datetime.now(timezone.utc)):
                assignments.append(assignment) 

    context= { # 2.we create a dictionary of items we want to send to the view.
        'milestoneCount' : len(milestones),
        'milestones' : milestones,
        'assignments' : assignments,

    }
    return render(request, 'milestone/milestone.html', context)

def getMilestone(request): 
    if request.method == 'GET':
        milestone = Milestone.objects.get(id=request.GET['id'])
        tasks = []
        mt = MilestoneTask.objects.filter(milestone_id=milestone.id)
        for t in mt:
            tasks.append(Task.objects.get(id=t.task_id))

        taskArray = []

        milestoneTasks = ''
        if len(tasks) > 0:
            for task in tasks:
                taskArray.append([task.id, task.name])
                milestoneTasks += '<div class="studyItem" onclick="getTask(' + str(task.id) + ')">\n'
                milestoneTasks += '<h2 class="studyItemTitle">' + task.name + '</h2>\n'
                milestoneTasks += '</div>\n'               
        
        context = {
            'id': milestone.id,
            'name':milestone.name,
            'set':milestone.set.strftime('%Y-%m-%dT%H:%M'),
            'due':milestone.due.strftime('%Y-%m-%dT%H:%M'),
            'assignment' : milestone.assignment.name,
            'taskOptions' : taskArray,
            'tasks' : milestoneTasks
        }
        return HttpResponse(json.dumps(context), content_type="application/json")
    return index(request)    

def getTasks(request):
    if request.method == 'GET':
        id=request.GET['id']
        tasks = Task.objects.filter(user_id=request.user, assignment_id=id)
        context = []
        for task in tasks:
            context.append([task.id, task.name])
    return HttpResponse(json.dumps(context), content_type="application/json")

def addTask(request):
    if request.method == 'GET':
        task = Task.objects.get(id=request.GET['id'])        
        taskItem = ''        
        taskItem += '<div class="studyItem" onclick="getTask(' + str(task.id) + ')">\n'
        taskItem += '<h2 class="studyItemTitle">' + task.name + '</h2>\n'
        taskItem += '</div>\n'

        context = {
            'task' : taskItem,
            'set': task.set.strftime('%Y-%m-%dT%H:%M'),
            'due': task.due.strftime('%Y-%m-%dT%H:%M'),
        }
        return HttpResponse(json.dumps(context), content_type="application/json")
    return index(request)

def saveMilestone(request):
    if request.method == 'GET':
        taskIDs = request.GET.getlist('tasks[]')
        print(taskIDs)
        if int(request.GET['id']) > 0:
            milestone = Milestone.objects.get(id=request.GET['id'])
            milestone.delete()
        newMilestone = Milestone(
            assignment = Assignment.objects.get(id=request.GET['assignment']),
            name = request.GET['name'],
            set = request.GET['set'],
            due = request.GET['due'],
            complete = 0,
            user = request.user
        )
        newMilestone.save()

        for taskID in taskIDs:
            newMT = MilestoneTask(
                milestone_id = newMilestone.id,
                task_id = taskID
            )
            newMT.save() 
        return HttpResponse(json.dumps("gg"), content_type="application/json")
    return index(request)

def deleteMilestone(request):
    if request.method == 'GET':
        selectedMilestone = Milestone.objects.get(id=request.GET['id'])
        selectedMilestone.delete()
    return HttpResponse(json.dumps("gg"), content_type="application/json")
