DELETE FROM root_usermodules;
DELETE FROM root_module;
DELETE FROM root_assignment;
DELETE FROM root_task;
DELETE FROM root_activity;
DELETE FROM root_milestone;
DELETE FROM root_milestonetask;
delete from sqlite_sequence where name='root_module';
delete from sqlite_sequence where name='root_assignment';
delete from sqlite_sequence where name='root_task';
delete from sqlite_sequence where name='root_activity';
delete from sqlite_sequence where name='root_taskactivity';
delete from sqlite_sequence where name='root_milestone';
delete from sqlite_sequence where name='root_usermodules';
delete from sqlite_sequence where name='root_milestonetask';


INSERT INTO root_module ("name", "set", "due")
VALUES ("Programming 2", "2019-01-15 00:00:00",  "2019-05-01 00:00:00");

INSERT INTO root_module ("name", "set", "due")
VALUES ("Graphics 1", "2019-01-15 00:00:00",  "2019-05-01 00:00:00");

INSERT INTO root_module ("name", "set", "due")
VALUES ("Data Structures & Algorithums", "2019-01-15 00:00:00",  "2019-05-01 00:00:00");

INSERT INTO root_module ("name", "set", "due")
VALUES ("Systems Engineering", "2019-01-15 00:00:00",  "2019-05-01 00:00:00");


INSERT INTO root_assignment ("module_id", "name", "weight", "set", "due", "complete")
VALUES  (1, "coursework 1", 15, "2019-05-01 11:30:45",  "2019-05-11 11:30:45", 0);

INSERT INTO root_assignment ("module_id", "name", "weight", "set", "due", "complete")
VALUES  (1, "coursework 2", 15, "2019-05-07 11:30:45",  "2019-05-11 11:30:45", 0);


INSERT INTO root_task ("assignment_id", "name", "note", "set", "due", "complete", "duration", "user_id")
VALUES  (1, "Plan", "none", "2019-05-01 11:30:45",  "2019-05-03 11:30:45", 1, "0", 1);

INSERT INTO root_task ("assignment_id", "name", "note", "set", "due", "complete", "duration", "user_id")
VALUES  (1, "Develop", "none", "2019-05-01 11:30:45",  "2019-05-03 11:30:45", 0, "0", 1);

INSERT INTO root_task ("assignment_id", "name", "note", "set", "due", "complete", "duration", "user_id")
VALUES  (1, "Test", "none", "2019-04-01 11:30:45",  "2019-04-03 11:30:45", 0, "0", 1);

INSERT INTO root_task ("assignment_id", "name", "note", "set", "due", "complete", "duration", "user_id")
VALUES  (1, "Submit", "none", "2019-05-01 11:30:45",  "2019-05-03 11:30:45", 0, "0", 1);


INSERT INTO root_activity("task_id", "name", "duration", "note", "complete", "user_id")
VALUES (1, "Create UML", "0","none", 0, 1);


INSERT INTO root_milestone("assignment_id", "name", "set", "due", "complete", "user_id")
VALUES (1, "Planing", "2019-05-01 11:30:45", "2019-05-01 11:30:45", 0, 1);


INSERT INTO root_milestonetask("milestone_id", "task_id")
VALUES (1,2);

INSERT INTO root_usermodules("user_id", "module_id")
VALUES (1,1);
INSERT INTO root_usermodules("user_id", "module_id")
VALUES (1,2);
INSERT INTO root_usermodules("user_id", "module_id")
VALUES (1,3);
INSERT INTO root_usermodules("user_id", "module_id")
VALUES (1,4);