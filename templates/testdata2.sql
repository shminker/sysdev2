DELETE FROM root_usermodules;
DELETE FROM root_module;
DELETE FROM root_assignment;
DELETE FROM root_task;
DELETE FROM root_activity;
DELETE FROM root_milestone;
DELETE FROM root_milestonetask;
ALTER SEQUENCE root_usermodules_id_seq RESTART WITH 1;
ALTER SEQUENCE root_module_id_seq RESTART WITH 1;
ALTER SEQUENCE root_assignment_id_seq RESTART WITH 1;
ALTER SEQUENCE root_task_id_seq RESTART WITH 1;
ALTER SEQUENCE root_activity_id_seq RESTART WITH 1;
ALTER SEQUENCE root_milestone_id_seq RESTART WITH 1;
ALTER SEQUENCE root_milestonetask_id_seq RESTART WITH 1;


INSERT INTO root_module ("name", "set", "due")
VALUES ('Programming 2', '2019-01-15 00:00:00',  '2019-07-01 00:00:00');
INSERT INTO root_module ("name", "set", "due")
VALUES ('Graphics 1', '2019-01-15 00:00:00',  '2019-07-01 00:00:00');
INSERT INTO root_module ("name", "set", "due")
VALUES ('Networks', '2019-01-15 00:00:00',  '2019-07-01 00:00:00');
INSERT INTO root_module ("name", "set", "due")
VALUES ('Electonics', '2019-01-15 00:00:00',  '2019-07-01 00:00:00');

INSERT INTO root_assignment ("module_id", "name", "weight", "set", "due", "complete")
VALUES  (1, 'Coursework 1', 20, '2019-05-01 11:30:45',  '2019-05-11 11:30:45', 1);
INSERT INTO root_assignment ("module_id", "name", "weight", "set", "due", "complete")
VALUES  (1, 'Coursework 2', 30, '2019-04-01 11:30:45',  '2019-05-14 11:30:45', 1);
INSERT INTO root_assignment ("module_id", "name", "weight", "set", "due", "complete")
VALUES  (1, 'Exam', 50, '2019-01-01 11:30:00',  '2019-06-01 13:30:00', 0);

INSERT INTO root_assignment ("module_id", "name", "weight", "set", "due", "complete")
VALUES  (2, 'Coursework 1', 60, '2019-05-01 11:30:45',  '2019-05-11 11:30:45', 1);
INSERT INTO root_assignment ("module_id", "name", "weight", "set", "due", "complete")
VALUES  (2, 'Exam', 40, '2019-01-01 11:30:00',  '2019-06-01 13:30:00', 0);

INSERT INTO root_assignment ("module_id", "name", "weight", "set", "due", "complete")
VALUES  (3, 'Coursework 1', 50, '2019-05-01 11:30:45',  '2019-05-11 11:30:45', 0);
INSERT INTO root_assignment ("module_id", "name", "weight", "set", "due", "complete")
VALUES  (3, 'Coursework 2', 50, '2019-04-01 11:30:45',  '2019-06-14 11:30:45', 0);


INSERT INTO root_assignment ("module_id", "name", "weight", "set", "due", "complete")
VALUES  (4, 'Coursework 1', 80, '2019-05-01 11:30:45',  '2019-06-11 11:30:45', 0);
INSERT INTO root_assignment ("module_id", "name", "weight", "set", "due", "complete")
VALUES  (4, 'Exam', 20, '2019-04-01 11:30:45',  '2019-05-14 11:30:45', 0);

