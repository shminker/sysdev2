DELETE FROM root_profile;
DELETE FROM root_semester;
DELETE FROM root_module;
DELETE FROM root_assignment;
DELETE FROM root_task;
DELETE FROM root_activity;
DELETE FROM root_taskactivity;
delete from sqlite_sequence where name='root_profile';
delete from sqlite_sequence where name='root_semester';
delete from sqlite_sequence where name='root_module';
delete from sqlite_sequence where name='root_assignment';
delete from sqlite_sequence where name='root_task';
delete from sqlite_sequence where name='root_activity';
delete from sqlite_sequence where name='root_taskactivity';


INSERT INTO root_profile ("name", "user", "password")
VALUES ("Gabriela Jota", "gj1", "1234");


INSERT INTO root_semester ("profile_id")
VALUES (1);


INSERT INTO root_module ("semester_id", "name")
VALUES (1, "Programming 2");

INSERT INTO root_module ("semester_id", "name")
VALUES (1, "Graphics 1");

INSERT INTO root_module ("semester_id", "name")
VALUES (1, "Data Structures & Algorithums");

INSERT INTO root_module ("semester_id", "name")
VALUES (1, "Systems Engineering");


INSERT INTO root_assignment ("module_id", "name", "weight", "set", "due", "complete")
VALUES  (1, "coursework 1", 15, "01-01-2019 00:00:00",  "01-02-2019 00:00:00", 0);

INSERT INTO root_task ("assignment_id", "name", "note", "set", "due", "complete")
VALUES  (1, "Plan", "none", "01-01-2019 00:00:00",  "01-02-2019 00:00:00", 1);

INSERT INTO root_task ("assignment_id", "name", "note", "set", "due", "complete")
VALUES  (1, "Develop", "none", "01-01-2019 00:00:00",  "01-02-2019 00:00:00", 0);

INSERT INTO root_task ("assignment_id", "name", "note", "set", "due", "complete")
VALUES  (1, "Test", "none", "01-01-2019 00:00:00",  "01-02-2019 00:00:00", 0);

INSERT INTO root_task ("assignment_id", "name", "note", "set", "due", "complete")
VALUES  (1, "Submit", "none", "01-01-2019 00:00:00",  "01-02-2019 00:00:00", 0);


INSERT INTO root_activity("name", "set", "due", "note", "complete")
VALUES ("Create UML", "01-01-2019 00:00:00",  "01-02-2019 00:00:00", "none", 0);


INSERT INTO root_milestone("assignment_id", "name", "set", "due", "complete")
VALUES (1, "Planing", "01-01-2019 00:00:00", "01-02-2019 00:00:00", 0);

INSERT INTO root_milestonetask("milestone_id", "task_id")
VALUES (1,1);
VALUES (1,2);

INSERT INTO root_taskactivity("task_id", "activity_id")
VALUES (1,1);
