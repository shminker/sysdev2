var taskName = document.getElementById("formName");
var taskAssignment = document.getElementById("formAssignment");
var taskSet = document.getElementById("formSet");
var taskDue = document.getElementById("formDue");
var taskNote = document.getElementById("formNotes");
var taskID = document.getElementById("taskID");

function getTask(id) {
    var csrftoken = $("[name=csrfmiddlewaretoken]").val();
    $.ajax({
        type:'GET',
        url:'getTask',
        headers:{
            "X-CSRFToken": csrftoken
        }, 
        data:{
            'id':id
        },
        success:function(data){
            //alert(data.name)
            var div = document.getElementById('taskActivities');
            div.innerText = "";
            taskName.value = data.name;
            taskName.disabled = true;
            taskAssignment.value = data.assignment;
            taskAssignment.disabled = true;
            taskSet.value = data.set;
            taskSet.disabled = true;
            taskDue.value = data.due;
            taskDue.disabled = true;
            taskID.value = data.id;
            taskID.disabled = true;
            taskNote.value = data.note;
            taskNote.disabled = true;
            div.innerHTML += data.activities;

        }
    })
}
function getActivity(id) {
    //alert(id);      
    window.open (
        "/activity/"+id,
        "mywindow",
        "menubar=0,resizable=0,width=600,height=800");
}

function addActivity() {
    id = document.getElementById('taskID').value;
    //alert("/activity/new/"+id);
    window.open (
        "/activity/new/"+id,
        "mywindow",
        "menubar=0,resizable=0,width=600,height=800");
}

function editTask(){
    taskName.disabled = false;
    taskAssignment.disabled = false;
    taskSet.disabled = false;
    taskDue.disabled = false;
    taskNote.disabled = false;
}

function saveEdit(){
    var csrftoken = $("[name=csrfmiddlewaretoken]").val();
    $.ajax({
        type:'GET',
        url:'edit/'+taskID.value,
        headers:{
            "X-CSRFToken": csrftoken
        }, 
        data:{
            'id':taskID.value,
            'name' : taskName.value,
            'set' : taskSet.value,
            'due' : taskDue.value,
            'note' : taskNote.value
        },
        success:function(data){
            if (data == "gg"){
                location.reload();
            }
        }
    })
}

function deleteTask(){
    var csrftoken = $("[name=csrfmiddlewaretoken]").val();
    $.ajax({
        type:'GET',
        url:'delete/'+taskID.value,
        headers:{
            "X-CSRFToken": csrftoken
        }, 
        data:{
            'id':taskID.value,
        },
        success:function(data){
            if (data == "gg"){
                location.reload();
            }
        }
    })
}

function reloader(){
    location.reload();
}    
