var demo_tasks = {
	"data":[
		{"id":1, "text":"Programming 2", "start_date":"01-01-2019", "duration":"43", "progress": 1, "open": true},
		{"id":2, "text":"coursework 1", "start_date":"05-01-2019", "duration":"10", "parent":"1", "progress":1.000000, "open": true},
		{"id":3, "text":"Planing", "start_date":"05-01-2019", "duration":"0", "parent":"2", "progress":1.000000, "open": true},
		{"id":4, "text":"Plan", "start_date":"05-01-2019", "duration":"2", "parent":"2", "progress":1.000000, "open": true},
		{"id":5, "text":"Develop", "start_date":"05-01-2019", "duration":"2", "parent":"2", "progress":1.000000, "open": true},
		{"id":6, "text":"Test", "start_date":"04-01-2019", "duration":"2", "parent":"2", "progress":1.000000, "open": true},
		{"id":7, "text":"Submit", "start_date":"05-01-2019", "duration":"2", "parent":"2", "progress":1.000000, "open": true},
		{"id":8, "text":"coursework 2", "start_date":"05-07-2019", "duration":"4", "parent":"1", "progress":1.000000, "open": true},
		{"id":9, "text":"Graphics 1", "start_date":"01-01-2019", "duration":"43", "progress": 1, "open": true},
		{"id":10, "text":"Data Structures & Algorithums", "start_date":"01-01-2019", "duration":"43", "progress": 1, "open": true},
		{"id":11, "text":"Systems Engineering", "start_date":"01-01-2019", "duration":"43", "progress": 1, "open": true},
],
"links":[
{"id":"1","source":"1","target":"2","type":"1"},{"id":"2","source":"2","target":"3","type":"1"},{"id":"3","source":"2","target":"4","type":"1"},{"id":"4","source":"2","target":"5","type":"1"},{"id":"5","source":"2","target":"6","type":"1"},{"id":"6","source":"2","target":"7","type":"1"},{"id":"7","source":"1","target":"8","type":"1"},	]
};