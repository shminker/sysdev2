ArrayList<String> links = new ArrayList<>();
int cnt = 0;
int linkCnt = 0;
String p = Paths.get(".").toAbsolutePath().normalize().toString() +
        "\\build\\classes\\concretestudy\\gc\\testdata.js";

PrintWriter w = new PrintWriter(p);
w.printf("%s\n\t%s\n",
        "var demo_tasks = {", "\"data\":[");
for (Module m : current_semester.getModules()){
    cnt++;
    int mCnt = cnt;
    w.printf("\t\t{\"id\":%d, \"text\":\"%s\", \"start_date\":\"%s\", \"duration\":\"%d\", \"progress\": 1, \"open\": true},\n",
            mCnt, m.getName(),m.getStartDate() , m.length());
    for (Assignment a : m.getAssginments()){
        cnt++;
        int aCnt = cnt;
        w.printf("\t\t{\"id\":%d, \"text\":\"%s\", \"start_date\":\"%s\", \"duration\":\"%d\", \"parent\":\"%d\", \"progress\":%f, \"open\": true},\n",
                cnt, a.getName(), a.getGCStart(), a.getGCDuration(), mCnt, a.getProgress());
        links.add(String.format("\t{\"id\":\"%d\",\"source\":\"%d\",\"target\":\"%d\",\"type\":\"%d\"},",
                ++linkCnt, mCnt, aCnt, 1));
        for (Task t : a.getTasks()){
            cnt++;
            w.printf("\t\t{\"id\":%d, \"text\":\"%s\", \"start_date\":\"%s\", \"duration\":\"%d\", \"parent\":\"%d\", \"progress\":%f, \"open\": true},\n",
                    cnt, t.getName(), t.getGCStart(), t.getGCDuration(), aCnt, t.getProgress());
            links.add(String.format("\t{\"id\":\"%d\",\"source\":\"%d\",\"target\":\"%d\",\"type\":\"%d\"},",
                ++linkCnt, aCnt, cnt, 1));
        }
    }
}
w.printf("],\n\"links\":[\n");
for(String link : links){
    w.println(link);
}
w.printf("\t]\n};");
w.close();
webView.getEngine().reload();
