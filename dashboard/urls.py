from django.urls import path
from . import views

urlpatterns = [
    path('', views.index, name='dashboard'),
    path('module/<moduleID>', views.getModule, name='dashboard'),
    path('assignment/<assignmentID>', views.getAssignment, name='dashboard')


]
