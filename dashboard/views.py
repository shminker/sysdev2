from django.shortcuts import render
from root.models import *
from datetime import datetime, timezone
from datetime import timedelta
from django.http import HttpResponseRedirect
import calendar
import os.path

import pytz


def index(request):
    current_user = request.user
    print(current_user)
    if (current_user.is_authenticated == False):
        return HttpResponseRedirect('account/login/')
    updateGanttChart(current_user, 0)
    tasks = Task.objects.filter(user_id=current_user)
    activities = Activity.objects.filter(user_id=current_user)
    assignments = []
    modules = UserModules.objects.filter(user_id=current_user)
    for module in modules:
        assign = Assignment.objects.filter(module_id=module.id)
        for assignment in assign:
            assignments.append(assignment)

    today = datetime.now(timezone.utc)

    complete = []
    upcomming = []
    missed = []


    for assignment in assignments:
        if assignment.complete == 1:
            complete.append(assignment)
        elif assignment.due > today:
            upcomming.append(assignment)
        else:
            missed.append(assignment)

    for task in tasks:
        if task.complete == 1:
            complete.append(task)
        elif task.due > today:
            upcomming.append(task)
        else:
            missed.append(task)

    for activity in activities:
        if activity.complete == 1:
            complete.append(activity)
        else:
            upcomming.append(activity)
        


    context= { # 2.we create a dictionary of items we want to send to the view.
        'day' : calendar.day_name[today.weekday()],
        'dayMonth' : today.day,
        'month' : calendar.month_name[today.month],
        'complete' : complete,
        'upcomming' : upcomming,
        'missed' : missed,
        'username' : request.user.username
    }
    return render(request, 'dashboard/dashboard.html', context)


def updateGanttChart(user, moduleID):
    modules = []
    if int(moduleID) > 0:
        ums = UserModules.objects.filter(user_id=user, module_id=moduleID)
    else:
        ums = UserModules.objects.filter(user_id=user)
    for um in ums:
        modules.append(Module.objects.get(id=um.module_id)) 

   

    cnt = 0
    lid = 0
    links = []
    dataFile = open(os.path.dirname(__file__) + '/../templates/gc/testdata.js', 'w')
    dataFile.write("%s\n\t%s\n" %
            ("var demo_tasks = {", "\"data\":["))
    for m in modules:
        cnt+=1
        mCnt = cnt
        dataFile.write("\t\t{\"id\":%d, \"text\":\"%s\", \"start_date\":\"%s\", \"duration\":\"%d\", \"progress\": 1, \"open\": true},\n" %
            (mCnt, m.name, "01-01-2019", 43))
        assignments = Assignment.objects.filter(module_id = m.id)
        for a in assignments:
            cnt+=1;
            aCnt = cnt;
            dataFile.write("\t\t{\"id\":%d, \"text\":\"%s\", \"start_date\":\"%s\", \"duration\":\"%d\", \"parent\":\"%d\", \"progress\":%f, \"open\": true},\n" %
                    (cnt, a.name, a.set.strftime("%m-%d-%Y"), getDuration(a.set, a.due), mCnt, 1))
            lid+=1        
            links.append(str("{\"id\":\"%d\",\"source\":\"%d\",\"target\":\"%d\",\"type\":\"1\"}," % 
            (lid, mCnt, aCnt)))
            milestones = Milestone.objects.filter(assignment_id = a.id, user_id = user)
            for mile in milestones:
                cnt+=1;
                dataFile.write("\t\t{\"id\":%d, \"text\":\"%s\", \"start_date\":\"%s\", \"duration\":\"%d\", \"parent\":\"%d\", \"progress\":%f, \"open\": true},\n" %
                        (cnt, mile.name, mile.set.strftime("%m-%d-%Y"), getDuration(mile.set, mile.due), aCnt, 1))
                lid+=1        
                links.append(str("{\"id\":\"%d\",\"source\":\"%d\",\"target\":\"%d\",\"type\":\"1\"}," % 
                (lid, aCnt, cnt)))
            tasks = Task.objects.filter(assignment_id = a.id, user_id = user)
            for task in tasks:
                cnt+=1;
                dataFile.write("\t\t{\"id\":%d, \"text\":\"%s\", \"start_date\":\"%s\", \"duration\":\"%d\", \"parent\":\"%d\", \"progress\":%f, \"open\": true},\n" %
                        (cnt, task.name, task.set.strftime("%m-%d-%Y"), getDuration(task.set, task.due), aCnt, 1))  
                lid+=1        
                links.append(str("{\"id\":\"%d\",\"source\":\"%d\",\"target\":\"%d\",\"type\":\"1\"}," % 
                (lid, aCnt, cnt)))
    dataFile.write("],\n\"links\":[\n")
    for link in links:
        dataFile.write(link)
    dataFile.write("\t]\n};")
    dataFile.close()
    return

def getDuration(set, due):
    due-set
    days = (due - set).days
    return days


def getModule(request, moduleID):
    current_user = request.user
    updateGanttChart(current_user, moduleID)
    activities = Activity.objects.filter(user_id=current_user)
    tasks = []
    activities = []

    assignments = []
    modules = UserModules.objects.filter(id=moduleID, user_id=current_user)
    for module in modules:
        assign = Assignment.objects.filter(module_id=module.id)
        for assignment in assign:
            assignments.append(assignment)

    for assignment in assignments:    
        taskAll = Task.objects.filter(user_id=current_user, assignment_id=assignment.id)
        for task in taskAll:
            tasks.append(task)
            activitiesAll = Activity.objects.filter(user_id=current_user, task_id=task.id)
            for activity in activitiesAll:
                activities.append(activity)


    today = datetime.now(timezone.utc)

    complete = []
    upcomming = []
    missed = []


    for assignment in assignments:
        if assignment.complete == 1:
            complete.append(assignment)
        elif assignment.due > today:
            upcomming.append(assignment)
        else:
            missed.append(assignment)

    for task in tasks:
        if task.complete == 1:
            complete.append(task)
        elif task.due > today:
            upcomming.append(task)
        else:
            missed.append(task)

    for activity in activities:
        if activity.complete == 1:
            complete.append(activity)
        else:
            upcomming.append(activity)
        


    context= { # 2.we create a dictionary of items we want to send to the view.
        'day' : calendar.day_name[today.weekday()],
        'dayMonth' : today.day,
        'month' : calendar.month_name[today.month],
        'complete' : complete,
        'upcomming' : upcomming,
        'missed' : missed,
        'module' : Module.objects.get(id=moduleID),
        'assignment' : len(assignments)
    }
    return render(request, 'dashboard/module.html', context)



def getAssignment(request, assignmentID):
    current_user = request.user
    assignment = Assignment.objects.get(id=assignmentID)
    updateGanttChart(current_user, assignment.module.id)
    activities = []
    tasks = Task.objects.filter(assignment_id=assignmentID, user_id=current_user)
    taskAll = Task.objects.filter(user_id=current_user, assignment_id=assignmentID)
    for task in taskAll:
        activitiesAll = Activity.objects.filter(user_id=current_user, task_id=task.id)
        for activity in activitiesAll:
            activities.append(activity)

    today = datetime.now(timezone.utc)

    complete = []
    upcomming = []
    missed = []

    for task in tasks:
        if task.complete == 1:
            complete.append(task)
        elif task.due > today:
            upcomming.append(task)
        else:
            missed.append(task)

    for activity in activities:
        if activity.complete == 1:
            complete.append(activity)
        else:
            upcomming.append(activity)
        


    context= { # 2.we create a dictionary of items we want to send to the view.
        'day' : calendar.day_name[today.weekday()],
        'dayMonth' : today.day,
        'month' : calendar.month_name[today.month],
        'complete' : complete,
        'upcomming' : upcomming,
        'missed' : missed,
        'assignment' : assignment
    }
    return render(request, 'dashboard/assignment.html', context)
